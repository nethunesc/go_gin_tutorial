package main

import (
	"fmt"
	"image/jpeg"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	uuid "github.com/nu7hatch/gouuid"
)

func main() {
	router := gin.Default()
	router.Static("./savedImages/", "savePath")
	router.POST("./savedImages/addImage", func(c *gin.Context) {
		data := c.Request.Body
		imageData, err := jpeg.Decode(data)
		if err != nil {
			log.Fatal(err)
		}

		saveUUID, err := uuid.NewV4()
		if err != nil {
			log.Fatal(err)
		}
		saveName := "./savedImages/" + saveUUID.String() + ".jpg"
		file, err := os.Create(saveName)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		var ops jpeg.Options
		ops.Quality = 1
		err = jpeg.Encode(file, imageData, &ops)
		if err != nil {
			log.Fatal(err)
		}
		c.String(http.StatusOK, fmt.Sprintf("'%s' saved!", saveUUID.String()))
	})

	router.Run("localhost:8000")
}
